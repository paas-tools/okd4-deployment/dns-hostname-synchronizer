# DNS Hostname Synchronizer

This is a simple golang script that performs a DNS Zone Transfer on a list of domains in order to compile a list of all reserved domains. Once all the
domains have been processed, specific number of `ReservedHostname` (this is controlled by `number-of-crs` argument) will be created in a namespace (controlled by `namespace` argument). Each of the created/updated resources will hold a list of used domain names. This can be used by different components to check whether a given domain already exists (without having to query the DNS).

**Mandatory values:**
- **namespace**: (e.g `myproject` ) The namespace where `ReservedHostname` resources will be created
- **dns-manager**: (e.g `192.0.0.0:53` ) The address of the DNS manager. This is used when performing AXFR (zone transfer)
- **tsig-keyname**: (e.g `myproject` ) TSIG key name
- **tsig-keydata**: (e.g `myproject` ) TSIG key data
- **domain**: (e.g. `webtest.cern.ch`) List of the domains for which we should perform a DNS zone transfer

**Optional values:**

- **kubeconfig**: Path to the kubeconfig file, only necessary if running outside of an OKD/K8S cluster.
- **number-of-crs**: Default value is `5`. This value is the total number of CRs that will be created to store existing/used hostnames. Entries are assigned to a CR using the `MD5Hash(alias) % number-of-crs`.

# ReservedHostname resource

The definition of `ReservedHostname` has been generated with `operator-sdk@1.8.0` and you need to remember to run `make generate && make manifests` after every change to `api/reservedhostname_types.go`.

`ReservedHostnames` resources are created based on the information stored in the DNS zones that are under our control. This component periodically performs a DNS zone transfer in order to retrieve all existing FQDNs.

## ReservedHostname naming

Each of the `ReservedHostname` resource derives its name from the cluster owning its `hostnames` (`.spec.hostnames`), e.g. `Route` created in the `webeos` cluster will end up in a `ReservedHostname/webeos-{number}`. By default we try to spread all the information across **5** `ReservedHostname`s (`ReservedHostname/cluster-name-1`, `ReservedHostname/cluster-name-2`, etc.) to prevent problems while creating such resources (there is the 1MB size limit of values stored in etcd, which constrains how big the JSON representation of a CR can be).

## Special `dns-hostname-synchronizer/owning-cluster` label

In order to keep the data stored in `ReservedHostnames`s consistent with the actual DNS zone information, we need to make sure that once a cluster is deprovisioned all outdated instances of `ReservedHostname`s are deleted from all the clusters where `dns-hostname-synchronizer` is running. To achieve that `dns-hostnames-synchronizer` sets a custom label (`dns-hostname-synchronizer/owning-cluster`) on the resources, with a value corresponding to the cluster where all domains included in `spec.hostnames` have been created. During startup, `dns-hostname-synchronizer` fetches the list of all `ReservedHostnames` that have this label and then removes the ones that contain deleted FQDNs. Using a custom label gives us the flexibility to have multiple CRs for each "owning" cluster if needed (e.g. `webeos-1`, `webeos-2` but both have same label `dns-hostname-synchronizer/owning-cluster: webeos` and the hostnames are distributed amongst the 2 CRs). This also ensures we won't delete `ReservedHostname`s created by other sources, which won't have the label specific to this `dns-hostname-synchronizer`.
