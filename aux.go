package main

import (
	"fmt"

	configv1 "github.com/openshift/api/config/v1"
	"github.com/openshift/library-go/pkg/config/helpers"
	"gitlab.cern.ch/paas-tools/okd4-deployment/dns-hostname-synchronizer/api/v1alpha1"

	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
)

// Type necessary to hold all the group flags that might be in the parameters
type arrayFlags []string

// Necessary function to be implemented
func (i *arrayFlags) String() string {
	return fmt.Sprint(*i)
}

func (i *arrayFlags) Set(value string) error {
	*i = append(*i, value)
	return nil
}

// patchStringValue specifies a patch operation for a string.
type patchStringValue struct {
	Op    string   `json:"op"`
	Path  string   `json:"path"`
	Value []string `json:"value"`
}

func newClientForConfig(kubeconfig string) *rest.RESTClient {
	var err error
	var config *rest.Config

	// creates cluster config
	config, err = helpers.GetKubeConfigOrInClusterConfig(kubeconfig, configv1.ClientConnectionOverrides{})
	if err != nil {
		panic(err.Error())
	}

	// Registering in the config the schema of the CRD
	v1alpha1.SchemeBuilder.AddToScheme(scheme.Scheme)

	// Setting up the group and version for client config
	config.ContentConfig.GroupVersion = &v1alpha1.GroupVersion
	config.APIPath = "/apis"
	config.NegotiatedSerializer = scheme.Codecs.WithoutConversion()
	config.UserAgent = rest.DefaultKubernetesUserAgent()

	// Creates the clientset
	clientset, err := rest.UnversionedRESTClientFor(config)
	if err != nil {
		panic(err.Error())
	}

	return clientset
}

func index(s []string, e string) int {
	for index, a := range s {
		if a == e {
			return index
		}
	}
	return -1
}

func removeByIndex(slice []string, s int) []string {
	return append(slice[:s], slice[s+1:]...)
}
