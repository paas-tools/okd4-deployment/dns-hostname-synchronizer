/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// ReservedHostnameSpec defines the desired state of ReservedHostname
type ReservedHostnameSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file

	// +kubebuilder:validation:Required
	// Hostnames array of hostnames currently being used by the old infrastructure
	Hostnames []string `json:"hostnames,omitempty"`
}

// ReservedHostnameStatus defines the observed state of ReservedHostname
type ReservedHostnameStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

// ReservedHostname is the Schema for the reservedhostnames API
type ReservedHostname struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   ReservedHostnameSpec   `json:"spec,omitempty"`
	Status ReservedHostnameStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// ReservedHostnameList contains a list of ReservedHostname
type ReservedHostnameList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []ReservedHostname `json:"items"`
}

func init() {
	SchemeBuilder.Register(&ReservedHostname{}, &ReservedHostnameList{})
}
