package main

import (
	"context"
	"crypto/md5"
	"encoding/binary"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"regexp"
	"strings"
	"time"

	"gitlab.cern.ch/paas-tools/okd4-deployment/dns-hostname-synchronizer/api/v1alpha1"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/rest"

	zaplogfmt "github.com/sykesm/zap-logfmt"
	uzap "go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"

	"github.com/miekg/dns"
)

const dnsSynchronizerLabel string = "dns-hostname-synchronizer/owning-cluster"
const reservedHostnamesPlural string = "reservedhostnames"

var (
	legacyOwnershipStrings = map[string]string{
		"heritage=external-dns,external-dns/owner=prod-web-internal": "legacy-webservices",
	}
	// When testing the performance of External-DNS we discovered that if we ask OpenShift to create a CR that holds 15000 endpoints
	// the request is considered too large. We picked 10 CRs to make sure there was enough space in each CR and that each one would be more or less 50% full.
	numberOfCrs = flag.Int("number-of-crs", 5, "Number of CRs that are going to be created to host all entries for a single cluster")
	kubeconfig  = flag.String("kubeconfig", "", "Path to Kubernetes config file")
	namespace   = flag.String("namespace", "", "Namespace where the CRs are going to be created")
	dnsManager  = flag.String("dns-manager", "", "The address of the DNS manager")
	tsigKeyName = flag.String("tsig-keyname", "", "TSIG key name")
	tsigKeyData = flag.String("tsig-keydata", "", "TSIG key data")
	// As of March 2022, our TSIG keys are in SHA-512
	tsigSigningAlgo = flag.String("tsig-signing-algo", "hmac-sha512.", "TSIG signing algorithm (supported values are hmac-sha1.,hmac-sha224.,hmac-sha256.,hmac-sha384.,hmac-sha512.,hmac-md5.sig-alg.reg.int.)")
	sharedDomains   arrayFlags
	kubeClient      *rest.RESTClient
	// The "_owner." prefix is the constant that is configured with external-dns (https://gitlab.cern.ch/paas-tools/okd4-deployment/dns-manager/-/blob/e8f3b6e0/chart/charts/external-dns/values.yaml#L89) (TODO: Update link).
	// If the txt-prefix changes in the deployment of `external-dns` we need to update it here as well.
	// External-dns has introduced a new format for TXT records <record_type>-foo.zone.org for versions >= v0.12.0: https://github.com/kubernetes-sigs/external-dns/blob/v0.13.2/docs/registry.md
	// This will match both old and new TXT ownership records.
	ownerDomainRegex *regexp.Regexp = regexp.MustCompile(`^_owner\.(?:cname-)?(.+\.cern\.ch)\.$`)
	// the following regex is based on how we build the `external-dns` owner-id string (https://gitlab.cern.ch/paas-tools/okd4-deployment/dns-manager/-/blob/284568c6/chart/charts/external-dns/templates/_helpers.tpl#L29)
	// as in case of the `ownerDomainRegex` we need to make sure that this regex is up-to-date with the `external-dns` configuration
	txtRecordOwnershipRegex *regexp.Regexp = regexp.MustCompile(`^(?P<clusterName>.*)-.*-.*-.*$`)
	log                                    = logf.Log.WithName("global")
)

func main() {
	flag.Var(&sharedDomains, "domain", "List of domains to perform the zone transfer on (can be used multiple times)")
	// Add the zap logger flag set to the CLI. The flag set should
	// be added before calling flag.Parse().
	opts := zap.Options{}
	opts.BindFlags(flag.CommandLine)
	flag.Parse()

	if *namespace == "" {
		panic("missing required argument -namespace")
	}

	if *dnsManager == "" {
		panic("missing required argument -dns-manager")
	}

	if *tsigKeyName == "" {
		panic("missing required argument -tsig-keyname")
	}

	if *tsigKeyData == "" {
		panic("missing required argument -tsig-keyname")
	}

	if len(sharedDomains) == 0 {
		panic("missing list of shared domains (-domain)")
	}

	kubeClient = newClientForConfig(*kubeconfig)

	// Format logs timestamp to time.RFC3339
	// Reference: https://sdk.operatorframework.io/docs/building-operators/golang/references/logging/#custom-zap-logger
	configLog := uzap.NewProductionEncoderConfig()
	configLog.EncodeTime = func(ts time.Time, encoder zapcore.PrimitiveArrayEncoder) {
		encoder.AppendString(ts.UTC().Format(time.RFC3339))
	}
	logfmtEncoder := zaplogfmt.NewEncoder(configLog)
	logger := zap.New(zap.UseFlagOptions(&opts), zap.Encoder(logfmtEncoder))
	logf.SetLogger(logger)

	// fetch a list of all existing ReservedHostnames in the cluster
	existingResources, err := listExistingReservedHostnames()
	if err != nil {
		panic(err.Error())
	}

	// get all registered domain names under each of the shared domains
	hostnames, err := getHostnamesFromDNS()
	if err != nil {
		panic(err.Error())
	}

	// iterate over the results while doing the following:
	// 1. create or update an instance of ReservedHostname
	// 2. if the resource's name is in the `existingResources` array, remove it - after this loop finishes `existingResources` will only
	//    contain the names of the ReservedHostnames that we should delete from the cluster
	for clusterName, domains := range hostnames {
		for _, resource := range generateCRs(clusterName, domains) {
			ind := index(existingResources, resource.ObjectMeta.Name)

			// there is already a `ReservedHostname` with that name, so we should simply update it
			if ind >= 0 {
				err = updateReservedHostname(resource)
				if err != nil {
					log.Error(err, fmt.Sprintf("Failed to update ReservedHostname/%s with %d endpoints.", resource.ObjectMeta.Name, len(resource.Spec.Hostnames)))
				} else {
					log.Info(fmt.Sprintf("Updated CR ReservedHostname/%s with %d endpoints", resource.ObjectMeta.Name, len(resource.Spec.Hostnames)))
				}

				existingResources = removeByIndex(existingResources, ind)
			} else {
				err = createReservedHostname(resource)
				if err != nil {
					log.Error(err, fmt.Sprintf("Failed to create ReservedHostname/%s with %d endpoints.", resource.ObjectMeta.Name, len(resource.Spec.Hostnames)))
				} else {
					log.Info(fmt.Sprintf("Created CR ReservedHostname/%s with %d endpoints", resource.ObjectMeta.Name, len(resource.Spec.Hostnames)))
				}
			}
		}
	}

	// remove ReservedHostnames from the cluster
	removeOrphanedReservedHostnamesByName(existingResources)
}

func getHostnamesFromDNS() (map[string][]string, error) {
	// A nested map of owningCluster -> hostnames -> anything
	// We only use the nested map of hostnames to ensure we get unique hostnames
	// because as of April 2023 external-dns generates multiple TXT ownership records
	// for each hostname.
	hostnames := make(map[string]map[string]interface{})

	for _, sharedDomain := range sharedDomains {
		transfer := &dns.Transfer{TsigSecret: map[string]string{*tsigKeyName: *tsigKeyData}}
		message := &dns.Msg{}
		if !strings.HasSuffix(sharedDomain, ".") {
			sharedDomain = sharedDomain + "."
		}

		message.SetAxfr(sharedDomain)
		message.SetTsig(*tsigKeyName, *tsigSigningAlgo, 300, time.Now().Unix())

		channel, err := transfer.In(message, *dnsManager)
		if err != nil {
			log.Error(err, fmt.Sprintf("Problem while doing a transfer of %s", sharedDomain))
			return nil, err
		}

		for item := range channel {
			if item.Error != nil {
				log.Error(item.Error, fmt.Sprintf("Problem while iterating over the channel"))
				return nil, item.Error
			} else {
				for _, entry := range item.RR {
					header := entry.Header()
					if header.Rrtype == dns.TypeTXT {
						txtRecord, ok := entry.(*dns.TXT)
						if !ok {
							log.V(6).Info(fmt.Sprintf("Record is not of type dns.TXT: %s", entry))
							continue
						}

						// we only care about specific TXT records
						isOwnershipRecord, domain := checkIfIsAnOwnershipRecord(txtRecord)
						if !isOwnershipRecord {
							log.V(6).Info(fmt.Sprintf("Skipping non-related record: %s", txtRecord.Header()))
							continue
						}

						// parse the DNS records contents in order to get the owning cluster
						owningCluster, err := getDomainOwnership(txtRecord)
						if err != nil {
							log.Error(err, "Error parsing owning cluster from DNS records")
							continue
						}

						if _, exists := hostnames[owningCluster]; !exists {
							hostnames[owningCluster] = make(map[string]interface{})
						}
						hostnames[owningCluster][domain] = nil
					}
				}
			}
		}
	}

	// return the map of owningCluster -> array of hostnames (i.e. the keys of the nested map)
	hostnamesPerCluster := map[string][]string{}
	for owningCluster, hostnamesMap := range hostnames {
		hostnamesKeys := make([]string, 0, len(hostnamesMap))
		for k := range hostnamesMap {
			hostnamesKeys = append(hostnamesKeys, k)
		}
		hostnamesPerCluster[owningCluster] = hostnamesKeys
	}
	return hostnamesPerCluster, nil
}

// this function checks if a given TXT record looks like an "ownership" record (e.g. _owner.example.app.cern.ch)
// this is based on the prefix which is configured in the deployment of `external-dns`
// https://gitlab.cern.ch/paas-tools/okd4-deployment/dns-manager/-/blob/master/chart/charts/external-dns/templates/_deployment.yaml#L52-53
func checkIfIsAnOwnershipRecord(txtRecord *dns.TXT) (bool, string) {
	match := ownerDomainRegex.FindStringSubmatch(txtRecord.Header().Name)
	if len(match) > 1 {
		return true, match[1]
	}
	return false, ""
}

// this function checks whether a given TXT record holds properly formatted information about the owning cluster
// e.g. _owner.example.app.cern.ch. 60 IN TXT "heritage=external-dns,external-dns/owner=paas-app-internal-route,external-dns/resource=route/example/example"
// this function assumes specific format of the record contents which is configured in the `external-dns` component
// https://gitlab.cern.ch/paas-tools/okd4-deployment/dns-manager/-/blob/master/chart/charts/external-dns/templates/_helpers.tpl#L26
func getDomainOwnership(txtRecord *dns.TXT) (string, error) {
	if len(txtRecord.Txt) == 0 {
		return "", fmt.Errorf("empty TXT record: %s", txtRecord.String())
	}

	// resources owned by one of the components listed in `legacyOwnershipStrings` are the DNS records from the legacy webservices
	if val, ok := legacyOwnershipStrings[txtRecord.Txt[0]]; ok {
		return val, nil
	}

	txtRecordValueParts := strings.Split(txtRecord.Txt[0], ",")
	if len(txtRecordValueParts) < 2 {
		return "", fmt.Errorf("incorrect value of the TXT record: %s", txtRecord.String())
	}

	ownershipParts := strings.Split(txtRecordValueParts[1], "=")
	if len(ownershipParts) != 2 {
		return "", fmt.Errorf("incorrect value of the ownership string: %s", txtRecord.String())
	}

	ownershipValue := ownershipParts[1]
	match := txtRecordOwnershipRegex.FindStringSubmatch(ownershipValue)
	if len(match) <= 1 {
		return "", fmt.Errorf("ownership string has wrong format: %s", ownershipValue)
	}

	return match[1], nil
}

func generateCRs(owningCluster string, domains []string) (rsvHostnames []*v1alpha1.ReservedHostname) {
	var hostnames = make([][]string, *numberOfCrs)

	for _, alias := range domains {
		// We will use the result of MD5Hash(alias) % numberOfCrs to determine in which CR we are going to store this record.
		// We do this to evenly distribute the different DNS records among the different CRs and to also avoid the problem
		// that if a record is removed we will for sure remove it from the CRs the next time this program runs
		hash := md5.Sum([]byte(alias))
		key := binary.BigEndian.Uint16(hash[:]) % uint16(*numberOfCrs)

		hostnames[key] = append(hostnames[key], alias)
	}

	rsvHostnames = make([]*v1alpha1.ReservedHostname, *numberOfCrs)
	for i := 0; i < *numberOfCrs; i++ {
		name := fmt.Sprintf("%s-%d", owningCluster, i+1)

		rsvHostname := &v1alpha1.ReservedHostname{
			ObjectMeta: metav1.ObjectMeta{
				Name: name,
				Labels: map[string]string{
					dnsSynchronizerLabel: owningCluster,
				},
			},
		}

		var hostnamesValue []string
		if hostnames[i] != nil {
			hostnamesValue = hostnames[i]
		} else {
			hostnamesValue = []string{}
		}

		rsvHostname.Spec = v1alpha1.ReservedHostnameSpec{
			Hostnames: hostnamesValue,
		}

		rsvHostnames[i] = rsvHostname
	}

	return
}

func listExistingReservedHostnames() ([]string, error) {
	var status int
	var existingResources []string

	result := kubeClient.Get().
		Namespace(*namespace).
		Resource(reservedHostnamesPlural).
		// we only care about the `ReservedHostnames` with a specific label
		Param("labelSelector", dnsSynchronizerLabel).
		Do(context.TODO())

	result.StatusCode(&status)
	if result.Error() != nil {
		return nil, result.Error()
	}

	if status != 200 {
		return nil, errors.New("error happened")
	}

	ls, err := result.Get()
	if err != nil {
		return nil, err
	}

	resources, err := runtime.DefaultUnstructuredConverter.ToUnstructured(ls)
	if err != nil {
		return nil, err
	}

	items := resources["items"].([]interface{})
	for _, item := range items {
		definition := item.(map[string]interface{})
		metadata := definition["metadata"].(map[string]interface{})
		resourceName := metadata["name"].(string)
		existingResources = append(existingResources, resourceName)
	}

	return existingResources, nil
}

func createReservedHostname(rsvHostname *v1alpha1.ReservedHostname) error {
	// Attempts to create the resource
	result := kubeClient.Post().
		Namespace(*namespace).
		Resource(reservedHostnamesPlural).
		Body(rsvHostname).
		Do(context.TODO())

	return result.Error()
}

func updateReservedHostname(rsvHostname *v1alpha1.ReservedHostname) error {
	payload := []patchStringValue{{
		Op:    "replace",
		Path:  "/spec/hostnames",
		Value: rsvHostname.Spec.Hostnames,
	}}

	toSend, err := json.Marshal(payload)
	if err != nil {
		panic(err)
	}

	result := kubeClient.Patch(types.JSONPatchType).
		Namespace(*namespace).
		Resource(reservedHostnamesPlural).
		Name(rsvHostname.Name).
		Body(toSend).
		Do(context.TODO())

	return result.Error()
}

func removeOrphanedReservedHostnamesByName(reservedHostnames []string) {
	for _, rsvHostname := range reservedHostnames {
		result := kubeClient.Delete().
			Namespace(*namespace).
			Resource(reservedHostnamesPlural).
			Name(rsvHostname).
			Do(context.TODO())

		if result.Error() != nil {
			log.Error(result.Error(), fmt.Sprintf("Error occurred while deleting ReservedHostname/%s", rsvHostname))
		} else {
			log.Info(fmt.Sprintf("Deleted ReservedHostname/%s from %s namespace", rsvHostname, *namespace))
		}
	}
}
