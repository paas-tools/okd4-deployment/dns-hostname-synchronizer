FROM registry.cern.ch/docker.io/library/golang:1.19 AS builder
WORKDIR /build

# Copy the Go modules
COPY go.mod go.sum ./
COPY vendor vendor

# Copy the go source
COPY main.go aux.go ./
COPY api/ api/

# Compile static Go binary
RUN CGO_ENABLED=0 go build -a -o dns-hostname-synchronizer

FROM registry.cern.ch/docker.io/library/busybox:stable

COPY --from=builder /build/dns-hostname-synchronizer /usr/bin/
ENTRYPOINT ["/usr/bin/dns-hostname-synchronizer"]
